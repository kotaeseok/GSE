#pragma once

#define MAX_OBJECT_COUNT 1000
#define GRAVITY 9.8

#define OBJ_TYPE_NORMAL 0
#define OBJ_TYPE_BULLET 1
#define OBJ_TYPE_HERO 2

struct GSEVec2
{
	float x, y = 0.f;
};

struct GSEVec3
{
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
};

struct GSEVec4
{
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
	float w = 0.f;
};
struct GSEKetBoardMapper
{
	bool W_key = false;
	bool A_key = false;
	bool S_key = false;
	bool D_key = false;

	bool UP_KEY = false;
	bool DOWN_KEY = false;
	bool RIGHT_KEY = false;
	bool LEFT_KEY = false;

};