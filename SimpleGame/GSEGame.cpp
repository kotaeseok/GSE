#include "stdafx.h"
#include "GSEGame.h"
#include "GSEObject.h"


std::random_device rd;
std::default_random_engine dre{ rd() };

std::uniform_int_distribution<int> uid(-1600, 1600);


GSEGame::GSEGame(GSEVec2 size)
{
	m_renderer = new Renderer((int)size.x, (int)size.y);
	m_objectMgr = new GSEObjectMgr();
	m_soundMgr = new Sound();

	//텍스처 생성

	m_HerotexID = m_renderer->GenPngTexture("./Image/Player_Idle.png");
	m_Herotex_att_ID = m_renderer->GenPngTexture("./Image/Player_Attack.png");
	
	m_BullettexID = m_renderer->GenPngTexture("./Image/Fire.png");
	m_Fire_ani_texID = m_renderer->GenPngTexture("./Image/Fire.png");
	m_bgtexID = m_renderer->GenPngTexture("./Image/bg.png");
	m_enemytexID = m_renderer->GenPngTexture("./Image/monster_Idle2.png");
	m_snowtexID= m_renderer->GenPngTexture("./Image/snow.png");
	m_FirePtexID = m_renderer->GenPngTexture("./Image/fireTex.png");
	//사운드 생성
	m_bgmSound = m_soundMgr->CreateBGSound("./Sounds/bgm.mp3");
	m_fireSound = m_soundMgr->CreateShortSound("./Sounds/fire.wav");
	m_CollSound = m_soundMgr->CreateShortSound("./Sounds/explosion.mp3");
	//파티클 생성

	m_particleClimate = m_renderer->CreateParticleObject(
		1000,
		-1024, -1024, 1024, 1024,
		3, 3, 7, 7,
		-3, -3, 3, 0
	);
	m_particleFire = m_renderer->CreateParticleObject(
		5,
		0, 0, 0, 0,
		3, 3, 3, 3,
		-10, -10, 10, 10
	);


	//create hero Obj
	GSEVec3 heroobjPos = { 0, 0, 0 };
	GSEVec3 heroobjSize = { 100, 100, 100 };
	GSEVec3 heroobjVel = { 0, 0, 0 };
	GSEVec3 heroobjAcc = { 0, 0, 0 };
	float heromass = 1.f;
	int heroHp = 1000;
	m_heroId = m_objectMgr->AddObject(heroobjPos, heroobjSize, heroobjVel, heroobjAcc, heromass);
	m_objectMgr->GetObject(m_heroId)->SetType(OBJ_TYPE_HERO);
	m_objectMgr->GetObject(m_heroId)->setHp(heroHp);
	m_objectMgr->GetObject(m_heroId)->setMaxHp(heroHp);
	m_objectMgr->GetObject(m_heroId)->setTextureID(m_HerotexID);
	m_objectMgr->GetObject(m_heroId)->setdrawGauge(true);


	m_soundMgr->PlayBGSound(m_bgmSound, true, 0.3f);
	//장애물
	/*GSEVec3 testobjPos = { 100, 0, 0 };
	GSEVec3 testobjSize = { 50, 50, 50 };
	GSEVec3 testobjVel = { 0, 0, 0 };
	GSEVec3 testobjAcc = { 0, 0, 0 };
	float testmass = 1.0f;
	int TestHp = 300;
	int testObjID = m_objectMgr->AddObject(testobjPos, testobjSize, testobjVel, testobjAcc, testmass);
	m_objectMgr->GetObject(testObjID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObjID)->setHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setdrawGauge(true);
	m_objectMgr->GetObject(testObjID)->setMaxHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setTextureID(m_enemytexID);
	testobjPos.x = 100;
	testobjPos.y = 100;
	testobjPos.z = 0;
	testObjID = m_objectMgr->AddObject(testobjPos, testobjSize, testobjVel, testobjAcc, testmass);
	m_objectMgr->GetObject(testObjID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObjID)->setHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setMaxHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setdrawGauge(true);
	m_objectMgr->GetObject(testObjID)->setTextureID(m_enemytexID);
	testobjPos.x = -100;
	testobjPos.y = -100;
	testobjPos.z = 0;
	testObjID = m_objectMgr->AddObject(testobjPos, testobjSize, testobjVel, testobjAcc, testmass);
	m_objectMgr->GetObject(testObjID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObjID)->setHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setMaxHp(TestHp);
	m_objectMgr->GetObject(testObjID)->setdrawGauge(true);
	m_objectMgr->GetObject(testObjID)->setTextureID(m_enemytexID);*/
	
	
	
	
	
	// test
	
	//for (int i = 0; i < MAX_OBJECT_COUNT-1; ++i)
	//{
	//	GSEVec3 objPos = { 0, 0, 0 };
	//	GSEVec3 objSize = { 100, 10, 10 };
	//	GSEVec3 objVel = { 100, 10, 10 };
	//	GSEVec3 objAcc = { 100, 10, 10 };
	//	float mass = -1.f;
	//	objPos.x = ((float)std::rand() / (float)RAND_MAX - 0.5f) * 2.f * 250.f;
	//	objPos.y = ((float)std::rand() / (float)RAND_MAX - 0.5f) * 2.f * 250.f;
	//	objSize.x = ((float)std::rand() / (float)RAND_MAX) * 5.f;

	//	objVel.x = ((float)std::rand() / (float)RAND_MAX - 0.5f) * 2.f * 3.f;
	//	objVel.y = ((float)std::rand() / (float)RAND_MAX - 0.5f) * 2.f * 3.f;
	//	objVel.z = 0.f;
	//	m_objectMgr->AddObject(objPos, objSize,objVel,objAcc,mass);
	//}
}

GSEGame::~GSEGame()
{
	if (m_renderer)
		delete m_renderer;
	if (m_objectMgr)
		delete m_objectMgr;

	if (m_HerotexID >= 0) {
		m_renderer->DeleteTexture(m_HerotexID);
	}
	if (m_BullettexID >= 0) {
		m_renderer->DeleteTexture(m_BullettexID);
	}
}

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.f, 1.f, 1.0f);
	m_renderer->DrawGround(
		0,0,0,
		3440,1440,1,
		1,1,1,1,
		m_bgtexID
	);
	// Renderer Test
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i)
	{
		GSEObject *temp = m_objectMgr->GetObject(i);
		if (temp != NULL) {
			GSEVec3 pos = temp->GetPos();
			GSEVec3 size = temp->GetSize();
			GSEVec4 color = temp->GetColor();
			int textureID = temp->GetTextureID();
			if (size.x > 0.0000001f)
			{
				if (textureID < 0) 
				{
					m_renderer->DrawSolidRect(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w);
				}
				else if (textureID == m_Fire_ani_texID)
				{
					float age = temp->GetAge();
					int currX = (int)(age * 5.f) % 5;

					m_renderer->DrawTextureRectAnim(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w,
						textureID,
						5,
						1,
						currX,
						1,
						false);
				}
				else if (textureID == m_HerotexID)
				{
					float age = temp->GetAge();
					int currX = (int)(age * 3.f) % 3;

					m_renderer->DrawTextureRectAnim(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w,
						textureID,
						3,
						2,
						currX,
						0,
						false);
				}
				else if (textureID == m_enemytexID)
				{
					float age = temp->GetAge();
					int currX = (int)(age * 2.f) % 2;

					m_renderer->DrawTextureRectAnim(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w,
						textureID,
						2,
						4,
						currX,
						1,
						false);
				}
				else if (textureID == m_Herotex_att_ID)
				{
					int curY = temp->GetDir();
					m_renderer->DrawTextureRectAnim(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w,
						textureID,
						1,
						4,
						0,
						curY,
						false);
					
				}
				else
				{
					m_renderer->DrawTextureRect(
						pos.x, pos.y, pos.z,
						size.x, size.y, size.z,
						color.x, color.y, color.z, color.w,
						textureID);
				}
				//m_renderer->DrawSolidRect(pos.x, pos.y, pos.z, size.x, color.x, color.y, color.z, color.w);
				bool bDrawGauge = temp->GetdrawGauge();
				if (bDrawGauge)
				{
					int MaxHp = temp->GetMaxHp();
					int Hp = temp->GetHp();
					float percent = 100.f * (float)Hp / (float)MaxHp;

					m_renderer->DrawSolidRectGauge(
						pos.x, pos.y, pos.z,
						0, size.y/2, 0,
						size.x, 5, 10,
						1, 0, 0, 1,
						(int)percent,
						false);
				}
			}
		}
	}
	float age = 0;
	if (m_objectMgr->GetObject(m_heroId) != NULL)
		age = m_objectMgr->GetObject(m_heroId)->GetAge();
	m_renderer->DrawParticle(
		m_particleClimate,
		0, 0, 0,
		3,
		1, 1, 1, 1,
		-100, -100,
		m_snowtexID,
		1.f,
		age);
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i)
	{
		if (m_objectMgr->GetObject(i)!=NULL &&m_objectMgr->GetObject(i)->GetType() == OBJ_TYPE_BULLET) {
			m_renderer->DrawParticle(
				m_particleFire,
				m_objectMgr->GetObject(i)->GetPos().x, m_objectMgr->GetObject(i)->GetPos().y, 0,
				3,
				1, 1, 1, 1,
				0, 0,
				m_FirePtexID,
				1.f,
				age);
		}
	}
	
}

void GSEGame::UpdateObject(GSEKetBoardMapper keyMapper,float eTime)
{
	spawnTime += eTime;
	if (spawnTime >= eTime*20) {
		spawnTime = 0;
		GSEVec3 MonsterobjPos = { uid(dre), 600, 0};
		GSEVec3 MonsterobjSize = { 50, 50, 50 };
		GSEVec3 MonsterobjVel = { 0, 0, 0 };
		GSEVec3 MonsterobjAcc = { 0, 0, 0 };
		float Monstermass = 1.0f;
		int MonsterHp = 300;
		int MonsterID = m_objectMgr->AddObject(MonsterobjPos, MonsterobjSize, MonsterobjVel, MonsterobjAcc, Monstermass);
		m_objectMgr->GetObject(MonsterID)->SetType(OBJ_TYPE_NORMAL);
		m_objectMgr->GetObject(MonsterID)->setHp(MonsterHp);
		m_objectMgr->GetObject(MonsterID)->setdrawGauge(true);
		m_objectMgr->GetObject(MonsterID)->setMaxHp(MonsterHp);
		m_objectMgr->GetObject(MonsterID)->setTextureID(m_enemytexID);
	}
	
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		GSEObject* temp = m_objectMgr->GetObject(i);
		if (temp != NULL && temp->GetType() == OBJ_TYPE_NORMAL)
		{
			GSEVec3 mosterForceDirection = {0.f,0.f,0.f};

			mosterForceDirection.y = -1.f;
			float movingmosterForce = 700.f;
			mosterForceDirection.y *= movingmosterForce;

			m_objectMgr->AddForce(i, mosterForceDirection, eTime);
		}
	}
	if (m_objectMgr->GetObject(m_heroId) != NULL) {

		if (keyMapper.W_key || keyMapper.D_key || keyMapper.S_key || keyMapper.A_key)
		{
			float movingHeroForce = 1000.f;
			GSEVec3 heroForceDirection;
			if (keyMapper.W_key)
			{
				heroForceDirection.y += 1.f;
			}
			if (keyMapper.A_key)
			{
				heroForceDirection.x -= 1.f;
			}
			if (keyMapper.S_key)
			{
				heroForceDirection.y -= 1.f;
			}
			if (keyMapper.D_key)
			{
				heroForceDirection.x += 1.f;
			}

			heroForceDirection.x *= movingHeroForce;
			heroForceDirection.y *= movingHeroForce;
			heroForceDirection.z *= movingHeroForce;

			m_objectMgr->AddForce(m_heroId, heroForceDirection, eTime);
		}
		//fire bullet
		bool b_canFire;

		b_canFire = m_objectMgr->GetObject(m_heroId)->CanFire();
		if (b_canFire == false)
			m_objectMgr->GetObject(m_heroId)->setTextureID(m_Herotex_att_ID);
		else
			m_objectMgr->GetObject(m_heroId)->setTextureID(m_HerotexID);

		if ((keyMapper.UP_KEY || keyMapper.DOWN_KEY || keyMapper.RIGHT_KEY || keyMapper.LEFT_KEY) && b_canFire)
		{


			GSEVec3  bulletForceDirection;
			float firingBulletForce = 1400.f;

			m_soundMgr->PlayShortSound(m_fireSound, false, 0.5f);
			m_objectMgr->GetObject(m_heroId)->setTextureID(m_Herotex_att_ID);
			if (keyMapper.UP_KEY)
			{
				m_objectMgr->GetObject(m_heroId)->setDir(UP);
				bulletForceDirection.y += 1.f;
			}
			if (keyMapper.LEFT_KEY)
			{
				m_objectMgr->GetObject(m_heroId)->setDir(LEFT);
				bulletForceDirection.x -= 1.f;
			}
			if (keyMapper.DOWN_KEY)
			{
				m_objectMgr->GetObject(m_heroId)->setDir(DOWN);
				bulletForceDirection.y -= 1.f;
			}
			if (keyMapper.RIGHT_KEY)
			{
				m_objectMgr->GetObject(m_heroId)->setDir(RIGHT);
				bulletForceDirection.x += 1.f;
			}

			bulletForceDirection.x *= firingBulletForce;
			bulletForceDirection.y *= firingBulletForce;
			bulletForceDirection.z *= firingBulletForce;
			if (FLT_EPSILON < std::fabs(bulletForceDirection.x) + std::fabs(bulletForceDirection.y) + std::fabs(bulletForceDirection.z))
			{

				GSEVec3 BulletobjPos = { 0, 0, 0 };
				GSEVec3 BulletobjSize = { 50, 50, 50 };
				GSEVec3 BulletobjVel = { 0, 0, 0 };
				GSEVec3 BulletobjAcc = { 0, 0, 0 };
				float Bulletmass = 0.1f;
				int bulletID = m_objectMgr->AddObject(BulletobjPos, BulletobjSize, BulletobjVel, BulletobjAcc, Bulletmass);
				if (bulletID >= 0) {
					GSEVec3 heroPos;
					heroPos = m_objectMgr->GetObject(m_heroId)->GetPos();
					m_objectMgr->GetObject(bulletID)->setTextureID(m_Fire_ani_texID);
					m_objectMgr->GetObject(bulletID)->setPos(heroPos);
					m_objectMgr->GetObject(bulletID)->setfricCoef(0.1f);

					m_objectMgr->GetObject(bulletID)->SetType(OBJ_TYPE_BULLET);
					m_objectMgr->AddForce(bulletID, bulletForceDirection, eTime/*0.1f*/);
					m_objectMgr->GetObject(bulletID)->SetParent(m_objectMgr->GetObject(m_heroId));
					m_objectMgr->GetObject(m_heroId)->ResetCoolTime();
					m_objectMgr->GetObject(bulletID)->setHp(200);
				}
			}


		}
	}
	//충돌추가
	bool TestResult[MAX_OBJECT_COUNT];
	std::memset(TestResult, 0, sizeof(bool) * MAX_OBJECT_COUNT);

	for (int i = 0; i < MAX_OBJECT_COUNT; ++i)
	{
		for (int j = i + 1; j < MAX_OBJECT_COUNT; ++j)
		{
			if (m_objectMgr->GetObject(i) == NULL || m_objectMgr->GetObject(j) == NULL)
				continue;
			GSEVec3 objASize = m_objectMgr->GetObject(i)->GetSize();
			GSEVec3 objAPos = m_objectMgr->GetObject(i)->GetPos();
			GSEVec3 objAMin = { objAPos.x - objASize.x / 2.f,
				objAPos.y - objASize.y / 2.f ,
				objAPos.z - objASize.z / 2.f };
			GSEVec3 objAMax = { objAPos.x + objASize.x / 2.f,
				objAPos.y + objASize.y / 2.f ,
				objAPos.z + objASize.z / 2.f };

			GSEVec3 objBSize = m_objectMgr->GetObject(j)->GetSize();
			GSEVec3 objBPos = m_objectMgr->GetObject(j)->GetPos();
			GSEVec3 objBMin = { objBPos.x - objBSize.x / 2.f,
				objBPos.y - objBSize.y / 2.f ,
				objBPos.z - objBSize.z / 2.f };
			GSEVec3 objBMax = { objBPos.x + objBSize.x / 2.f,
				objBPos.y + objBSize.y / 2.f ,
				objBPos.z + objBSize.z / 2.f };
			bool isCollide = BBCollision(objAMin, objAMax, objBMin, objBMax);
			bool isParent = (m_objectMgr->GetObject(i)->isAncester(m_objectMgr->GetObject(j)))||
							(m_objectMgr->GetObject(j)->isAncester(m_objectMgr->GetObject(i)));
			
			 bool isSame = m_objectMgr->GetObject(i)->GetType() == m_objectMgr->GetObject(j)->GetType();
			
			 if (isCollide&&!isParent&&!isSame)
			{
				GSEObject* A1 = m_objectMgr->GetObject(i);
				GSEObject* A2 = m_objectMgr->GetObject(j);
				float m1 = A1->GetMass();
				float m2 = A2->GetMass();
				GSEVec3 vel1 = A1->GetVel();
				GSEVec3 vel2 = A2->GetVel();
				float finalVelX1 = ((m1 - m2) / (m1 + m2)) * vel1.x + ((2.f * m2) / (m1 + m2)) * vel2.x;
				float finalVelY1 = ((m1 - m2) / (m1 + m2)) * vel1.y + ((2.f * m2) / (m1 + m2)) * vel2.y;

				float finalVelX2 = ((2.f * m1) / (m1 + m2)) * vel1.x + ((m2-m1) / (m1 + m2)) * vel2.x;
				float finalVelY2= ((2.f * m1) / (m1 + m2)) * vel1.y + ((m2 - m1) / (m1 + m2)) * vel2.y;

				GSEVec3 temp = { finalVelX1,finalVelY1,vel1.z };
				A1->setVel(temp);

				GSEVec3 temp1 = { finalVelX2,finalVelY2,vel2.z };
				A2->setVel(temp1);

				int Hp1 = A1->GetHp();
				int Hp2 = A2->GetHp();

				A1->setHp(Hp1 - Hp2);
				A2->setHp(Hp2 - Hp1);

				TestResult[i]=true;
				TestResult[j] = true;

				m_soundMgr->PlayShortSound(m_CollSound, false, 0.3f);
			}

		}
	}
	for(int i =0;i<MAX_OBJECT_COUNT;++i)
	{
		if (m_objectMgr->GetObject(i) != NULL) {
			if (TestResult[i])
			{
				GSEVec4 colColor = { 1,0,0,1 };
				m_objectMgr->GetObject(i)->setColor(colColor);

			}
			else
			{
				GSEVec4 colColor = { 1,1,1,1 };
				m_objectMgr->GetObject(i)->setColor(colColor);
			}
		}
	}
	if (m_objectMgr != NULL) {
		m_objectMgr->UpdateObject(eTime);

	}
	else
	{
		std::cout << "m_objectMgr is NULL" << std::endl;
	}
	GSEVec3 heroPos;
	if(m_objectMgr->GetObject(m_heroId)!=NULL)
		heroPos = m_objectMgr->GetObject(m_heroId)->GetPos();

	if(heroPos.x>=-1200&& heroPos.x <1200)
		m_renderer->SetCameraPos(heroPos.x, 0+200);


	m_objectMgr->DoGarbageCollet();
}

bool GSEGame::BBCollision(GSEVec3 minA, GSEVec3 MaxA, GSEVec3 minB, GSEVec3 MaxB)
{
	if (minA.x > MaxB.x)
		return false;
	if (MaxA.x < minB.x)
		return false;
	if (minA.y > MaxB.y)
		return false;
	if (MaxA.y < minB.y)
		return false;
	if (minA.x > MaxB.x)
		return false;
	if (minA.z > MaxB.z)
		return false;
	if (MaxA.z < minB.z)
		return false;


	return true;
}
