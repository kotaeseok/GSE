#pragma once
#include "Global.h"
class GSEObject
{
public:
	GSEObject(GSEVec3 pos, GSEVec3 size, GSEVec3 vel, GSEVec3 acc,float mass);
	~GSEObject();

	GSEVec3 GetPos();
	void setPos(GSEVec3 in);
	GSEVec3 GetSize();
	void setSize(GSEVec3 in);

	GSEVec3 GetVel();
	void setVel(GSEVec3 in);
	GSEVec3 GetAcc();
	void setAcc(GSEVec3 in);

	GSEVec4 GetColor();
	void setColor(GSEVec4 in);

	float GetMass();
	void setMass(float in);

	float GetfricCoef();
	void setfricCoef(float in);
	int GetType();
	void SetType(int in);
	void Update(float eTime);

	float GetCoolTime();
	void SetCoolTime(float in);

	void AddForce(GSEVec3 force, float eTime);

	bool CanFire();
	void ResetCoolTime();

	void SetParent(GSEObject* in);
	bool isAncester(GSEObject* in);

	int GetHp();
	void setHp(int in);

	int GetMaxHp();
	void setMaxHp(int in);

	int GetTextureID();
	void setTextureID(int in);

	bool GetdrawGauge();
	void setdrawGauge(bool in);

	float GetAge();
	void setAge(float in);

	Dir GetDir();
	void setDir(Dir in);



private:
	GSEVec3 m_pos;
	GSEVec3 m_size;
	GSEVec3 m_vel;
	GSEVec3 m_acc;
	float m_mass;
	float m_fricCoef;
	int m_type;
	float m_coolTime;
	float m_coolTime_remaining;
	GSEVec4 m_Color;
	GSEObject* m_parent = NULL;


	Dir m_attDir = UP;
	int m_HP = 0;
	int m_MaxHP = 0;
	int m_textureID = -1;
	bool m_drawGauge = false;

	float m_Age = 0.f;
};

