#pragma once
#include "Global.h"
#include "GSEObject.h"
#include <iostream>
class GSEObjectMgr
{
public:
	GSEObjectMgr();
	~GSEObjectMgr();

	int AddObject(GSEVec3 pos, GSEVec3 size,GSEVec3 vel, GSEVec3 acc, float mass);
	GSEObject *GetObject(int index);

	bool DeleteObject(int index);
	int FindEmptySlot();

	void UpdateObject(float eTime);

	void AddForce(int index,GSEVec3, float eTime);

	void DoGarbageCollet();
private:
	GSEObject* m_object[MAX_OBJECT_COUNT];

	bool initPlayer = false;
};

