#include "stdafx.h"
#include "GSEObjectMgr.h"

GSEObjectMgr::GSEObjectMgr() {
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i)
	{
		m_object[i] = NULL;
	}
}
GSEObjectMgr::~GSEObjectMgr() 
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) 
	{
		DeleteObject(i);
	}
}

int GSEObjectMgr::AddObject(GSEVec3 pos, GSEVec3 size,GSEVec3 vel, GSEVec3 acc, float mass)
{
	if (!initPlayer) {
		int index = 0;
		m_object[index] = new GSEObject(pos, size, vel, acc, mass);
		initPlayer = true;
		return index;
	}
	else
	{
		int index = FindEmptySlot();
		if (index < 0)
		{
			std::cout << "no more empty obj slot" << std::endl;
			return -1;
		}
		m_object[index] = new GSEObject(pos, size, vel, acc, mass);
		return index;
	}


}

GSEObject *GSEObjectMgr::GetObject(int index)
{

	if (m_object[index] != NULL) 
	{
		return m_object[index];
	}
	//log required
	
	return NULL;
}

bool GSEObjectMgr::DeleteObject(int index)
{
	if (m_object[index]) {
		delete m_object[index];
		m_object[index] = NULL;
		return true;
	}

	return false;
}

int GSEObjectMgr::FindEmptySlot()
{
	for (int i = 1; i < MAX_OBJECT_COUNT; ++i) {
		if (m_object[i] == NULL)
		{
			return i;
		}
	}
	//���н� 
	return -1;
}

void GSEObjectMgr::UpdateObject(float eTime)
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_object[i] != NULL)
		{
			m_object[i]->Update(eTime);
		}
	}

}

void GSEObjectMgr::AddForce(int index,GSEVec3 force, float eTime)
{
	if (m_object[index] != NULL)
	{
		m_object[index]->AddForce(force, eTime);
	}

}

void GSEObjectMgr::DoGarbageCollet()
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_object[i] != NULL)
		{
			//check destroy
			
			//bullet type,vel
			GSEVec3 vel = m_object[i]->GetVel();
			float size = std::sqrtf(vel.x * vel.x + vel.y * vel.y);
			int type = m_object[i]->GetType();
			if (size < FLT_EPSILON&& type ==OBJ_TYPE_BULLET) {
				DeleteObject(i);
			}
			//delete
		}
		if (m_object[i] != NULL)
		{
			
			int Hp = m_object[i]->GetHp();
			if (Hp <= 0 ) {
				DeleteObject(i);
			}
			//delete
		}
		if (m_object[i] != NULL)
		{
			int type = m_object[i]->GetType();

			int Hp = m_object[i]->GetHp();
			if (type == OBJ_TYPE_NORMAL&& m_object[i]->GetPos().y<= -270.f) {
				DeleteObject(i);
			}
			//delete
		}
	}


}
