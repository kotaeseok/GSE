/*
Copyright 2022 Lee Taek Hee (Tech University of Korea)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "Renderer.h"
#include "GSEGame.h"
#include "GSEObject.h"
#include "Global.h"
GSEGame* g_game = NULL;
float g_prevTime = 0;
GSEKetBoardMapper g_keyMapper;
 
void RenderScene(void)
{
	// Renderer Test
	float eTime = glutGet(GLUT_ELAPSED_TIME) - g_prevTime;
	g_prevTime = glutGet(GLUT_ELAPSED_TIME);
	eTime = eTime / 1000;
	eTime = 0.016f;
	g_game->UpdateObject(g_keyMapper,eTime);
	g_game->RenderScene();
	glutSwapBuffers();
}

void Idle(void)
{
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w' | 'W':
		g_keyMapper.W_key = true;
		break;
	case 'a' | 'A':
		g_keyMapper.A_key = true;
		break;
	case 's' | 'S':
		g_keyMapper.S_key = true;
		break;
	case 'd'|'D':
		g_keyMapper.D_key = true;
		break;
	default:
		break;
	}
	//RenderScene();
}

void SpecialKeyInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_keyMapper.UP_KEY = true;
		break;
	case GLUT_KEY_DOWN:
		g_keyMapper.DOWN_KEY = true;
		break;
	case GLUT_KEY_RIGHT:
		g_keyMapper.RIGHT_KEY = true;
		break;
	case GLUT_KEY_LEFT:
		g_keyMapper.LEFT_KEY = true;
		break;
	default:
		break;
	}
	//RenderScene();
}

void KeyUpInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w' | 'W':
		g_keyMapper.W_key = false;
		break;
	case 'a' | 'A':
		g_keyMapper.A_key = false;
		break;
	case 's' | 'S':
		g_keyMapper.S_key = false;
		break;
	case 'd' | 'D':
		g_keyMapper.D_key = false;
		break;
	default:
		break;
	}
	//RenderScene();
}

void SpecialKeyUpInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_keyMapper.UP_KEY = false;
		break;
	case GLUT_KEY_DOWN:
		g_keyMapper.DOWN_KEY = false;
		break;
	case GLUT_KEY_RIGHT:
		g_keyMapper.RIGHT_KEY = false;
		break;
	case GLUT_KEY_LEFT:
		g_keyMapper.LEFT_KEY = false;
		break;
	default:
		break;
	}
	//RenderScene();
}



int main(int argc, char** argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	// Initialize Renderer
	GSEVec2 temp = { 1024.f, 1024.f };
	g_game = new GSEGame(temp);


	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	
	glutMouseFunc(MouseInput);

	glutKeyboardFunc(KeyInput); //키보드 다운
	glutSpecialFunc(SpecialKeyInput);

	glutKeyboardUpFunc(KeyUpInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_prevTime = glutGet(GLUT_ELAPSED_TIME);
	glutMainLoop();

	delete g_game;
	return 0;
}

