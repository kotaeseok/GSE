#pragma once
#include "Global.h"
#include "Renderer.h"
#include "GSEObjectMgr.h"
#include "Sound.h"
class GSEGame
{
public:
	GSEGame(GSEVec2 size);
	~GSEGame();
	void RenderScene();
	void UpdateObject(GSEKetBoardMapper keyMapper,float eTime);
	bool BBCollision(GSEVec3 minA, GSEVec3 MaxA, GSEVec3 minB, GSEVec3 MaxB);
private:
	Renderer* m_renderer = NULL;
	GSEObjectMgr* m_objectMgr= NULL;
	Sound* m_soundMgr = NULL;

	float spawnTime = 0.f;

	int m_heroId = -1;
	int m_HerotexID = -1;
	int m_Herotex_att_ID = -1;
	int m_BullettexID = -1;
	int m_Fire_ani_texID = -1;
	int m_enemytexID = -1;
	int m_snowtexID = -1;
	int m_bgtexID = -1;
	int m_FirePtexID = -1;
	

	//사운드
	int m_bgmSound = -1;
	int m_fireSound = -1;
	int m_CollSound = -1;

	//파티클

	int m_particleClimate = -1;
	int m_particleFire = -1;
};

