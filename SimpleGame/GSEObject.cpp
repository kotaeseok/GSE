#include "stdafx.h"
#include "GSEObject.h"
#include <float.h>
GSEObject::GSEObject(GSEVec3 pos, GSEVec3 size, GSEVec3 vel, GSEVec3 acc,float mass)
{
	m_pos = pos;
	m_size = size;
	m_vel = vel;
	m_acc = acc;
	m_mass = mass;
	m_fricCoef = 1.f;
	m_coolTime =0.5f;
	m_coolTime_remaining =0.f;

	m_type = OBJ_TYPE_NORMAL;
}

GSEObject::~GSEObject()
{

}

GSEVec3 GSEObject::GetPos()
{
	return m_pos;
}

void GSEObject::setPos(GSEVec3 in)
{
	m_pos = in;
}

GSEVec3 GSEObject::GetSize()
{
	return m_size;
}

void GSEObject::setSize(GSEVec3 in)
{
	m_size = in;
}

GSEVec3 GSEObject::GetVel()
{
	return m_vel;
}

void GSEObject::setVel(GSEVec3 in)
{
	m_vel = in;
}

GSEVec3 GSEObject::GetAcc()
{
	return m_acc;
}

void GSEObject::setAcc(GSEVec3 in)
{
	m_acc = in;
}

GSEVec4 GSEObject::GetColor()
{
	return m_Color;
}

void GSEObject::setColor(GSEVec4 in)
{
	this->m_Color = in;
}

float GSEObject::GetMass()
{
	return m_mass;
}

void GSEObject::setMass(float in)
{
	m_mass = in;
}

float GSEObject::GetfricCoef()
{
	return m_fricCoef;
}

void GSEObject::setfricCoef(float in)
{
	m_fricCoef = in;
}

int GSEObject::GetType()
{
	return m_type;
}

void GSEObject::SetType(int in)
{
	m_type = in;
}

void GSEObject::Update(float eTime)
{
	float normalForce = m_mass * (float)GRAVITY;
	float fric = m_fricCoef * normalForce;


	float mag =sqrtf( m_vel.x * m_vel.x + m_vel.y * m_vel.y);
	if (mag > FLT_EPSILON) {
		GSEVec2 velDir = { m_vel.x / mag , m_vel.y / mag };
		velDir.x *= -1.f;
		velDir.y *= -1.f;

		velDir.x *= fric;
		velDir.y *= fric;

		velDir.x /= m_mass;
		velDir.y /= m_mass;

		GSEVec2 resultVel = { 0,0 };

		resultVel.x = m_vel.x + velDir.x;
		resultVel.y = m_vel.y + velDir.y;
		if (resultVel.x * m_vel.x < 0.f) {
			m_vel.x = 0.f;
		}
		else {
			m_vel.x += velDir.x;
		}
		if (resultVel.y * m_vel.y < 0.f) {
			m_vel.y = 0.f;
		}
		else {
			m_vel.y += velDir.y;
		}

		
	}
	if (m_type == OBJ_TYPE_HERO) {
		if (m_pos.x + m_vel.x * eTime <1600 && m_pos.x + m_vel.x * eTime > -1600) {
			m_pos.x = m_pos.x + m_vel.x * eTime;

		}
		else {
			m_pos.x = m_pos.x - m_vel.x * eTime;
			m_vel.x = 0;
		}

		if (m_pos.y + m_vel.y * eTime < 380 && m_pos.y + m_vel.y * eTime > -200) {
			m_pos.y = m_pos.y + m_vel.y * eTime;

		}
		else {
			m_pos.y = m_pos.y - m_vel.y * eTime;
			m_vel.y = 0;
		}
	}
	m_pos.x = m_pos.x + m_vel.x * eTime;
	m_pos.y = m_pos.y + m_vel.y * eTime;
	m_pos.z = m_pos.z + m_vel.z * eTime;

	m_coolTime_remaining -= eTime;
	if (m_coolTime_remaining < FLT_EPSILON)
	{
		m_coolTime_remaining = 0.f;
	}
	
	//age
	m_Age += eTime;
}

float GSEObject::GetCoolTime()
{
	return m_coolTime;
}

void GSEObject::SetCoolTime(float in)
{
	m_coolTime = in;
}

void GSEObject::AddForce(GSEVec3 force, float eTime)
{
	GSEVec3 acc;
	acc.x = force.x / m_mass;
	acc.y = force.y / m_mass;
	acc.z = force.z / m_mass;
	m_vel.x += acc.x * eTime;
	m_vel.y += acc.y * eTime;
	m_vel.z += acc.z * eTime;

}

bool GSEObject::CanFire()
{
	if(m_coolTime_remaining<FLT_EPSILON)
	{
		return true;
	}
	return false;
}

void GSEObject::ResetCoolTime()
{
	m_coolTime_remaining = m_coolTime;
}

void GSEObject::SetParent(GSEObject* in)
{
	m_parent = in;
}

bool GSEObject::isAncester(GSEObject* in)
{
	if (in == m_parent)
	{
		return true;
	}
	return false;
}

int GSEObject::GetHp()
{
	return m_HP;
}

void GSEObject::setHp(int in)
{
	m_HP = in;
}

int GSEObject::GetMaxHp()
{
	return m_MaxHP;
}

void GSEObject::setMaxHp(int in)
{
	m_MaxHP = in;
}

int GSEObject::GetTextureID()
{
	return this->m_textureID;
}

void GSEObject::setTextureID(int in)
{
	this->m_textureID = in;
}

bool GSEObject::GetdrawGauge()
{
	return m_drawGauge;
}

void GSEObject::setdrawGauge(bool in)
{
	m_drawGauge = in;
}

float GSEObject::GetAge()
{
	return m_Age;
}

void GSEObject::setAge(float in)
{
	m_Age = in;
}

Dir GSEObject::GetDir()
{
	return m_attDir;
}

void GSEObject::setDir(Dir in)
{
	m_attDir = in;
}
